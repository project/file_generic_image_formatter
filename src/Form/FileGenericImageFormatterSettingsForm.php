<?php

namespace Drupal\file_generic_image_formatter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class FileGenericImageFormatterSettingsForm extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'file_generic_image_formatter_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('file_generic_image_formatter.settings');

    $form['icon_base_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Icon Base uri'),
      '#default_value' => $config->get('icon_base_uri'),
      '#required' => TRUE,
    ];
    $form['default_thumbnail_filename'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default thumbnail filename'),
      '#default_value' => $config->get('default_thumbnail_filename'),
      '#required' => TRUE,
    ];
    $form['info'] = [
      '#type' => 'item',
      '#title' => $this->t('Remember to rename or move the images files according those settings'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('file_generic_image_formatter.settings')
      ->set('icon_base_uri', $form_state->getValue('icon_base_uri'))
      ->set('default_thumbnail_filename', $form_state->getValue('default_thumbnail_filename'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['file_generic_image_formatter.settings'];
  }

}
