<?php

namespace Drupal\file_generic_image_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\file\FileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 * @FieldFormatter(
 *   id = "file_generic_image_formatter",
 *   label = @Translation("Generic image formatter"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class GenericImageFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  protected ConfigFactoryInterface $configFactory;

  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, ConfigFactoryInterface $config_factory) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->configFactory = $config_factory;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('config.factory')
    );
  }

  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $file = $item->entity;
      $thumbnail_uri = $this->getThumbnail($file);
      $element[$delta] = [
        '#theme' => 'image_style',
        '#uri' => $thumbnail_uri ?: $this->getDefaultThumbnailUri(),
        '#alt' => 'generic thumbnail',
        '#title' => 'generic thumbnail',
        '#style_name' => 'media_library',
      ];
    }

    return $element;
  }

  /**
   * @see \Drupal\media\Plugin\media\Source\File::getThumbnail()
   */
  protected function getThumbnail(FileInterface $file): ?string {
    $icon_base = $this->configFactory->get('file_generic_image_formatter.settings')
      ->get('icon_base_uri');

    // We try to automatically use the most specific icon present in the
    // $icon_base directory, based on the MIME type. For instance, if an
    // icon file named "pdf.png" is present, it will be used if the file
    // matches this MIME type.
    $mimetype = $file->getMimeType();
    $mimetype = explode('/', $mimetype);

    $icon_names = [
      $mimetype[0] . '--' . $mimetype[1],
      $mimetype[1],
      $mimetype[0],
    ];
    foreach ($icon_names as $icon_name) {
      $thumbnail = $icon_base . '/' . $icon_name . '.png';
      if (is_file($thumbnail)) {
        return $thumbnail;
      }
    }

    return NULL;
  }

  protected function getDefaultThumbnailUri(): string {
    $config = $this->configFactory->get('file_generic_image_formatter.settings');
    return $config->get('icon_base_uri') . '/' . $config->get('default_thumbnail_filename');
  }

}
